interface ISceneMessage {
  Head: string;
  TextLines: string[];
  Audio: IAudioMessage;
}

interface IAudioMessage {
  key: string;
  src: string;
}

interface IMainSettings {
  ExitButtonUrl: string;
  StartSceneMessage: ISceneMessage;
  WinSceneMessage: ISceneMessage;
  LoseSceneMessage: ISceneMessage;
}

interface IGameSettings {
  InstructionMessage: {
    Text: string;
    Audio: IAudioMessage;
  }
}

declare module '*.png';