/// <reference path="./../../@types/index.d.ts" />

import Phaser from 'phaser';
import { GameTimer } from './../../shared/helper/timer';
import { winScreen } from './../../shared/helper/win';
import BgUrl from './../image/bg.png';
import BiedronkaUrl from './../image/biedronka.png';
import BiegaczUrl from './../image/biegacz.png';
import RusalkaUrl from './../image/rusalka.png';
import TrzmielUrl from './../image/trzmiel.png';
declare const GameSettings: IGameSettings;

export default class GameScene extends Phaser.Scene {
  constructor() {
    super('GameScene');
  }

  private gameTimer;
  private instructionAudio: Phaser.Sound.BaseSound;

  private clicked: number;
  private clickedSuccess: number;

  preload() {
    this.load.image('game-scene-bg', BgUrl);
    this.load.image('game-biedronka', BiedronkaUrl);
    this.load.image('game-biegacz', BiegaczUrl);
    this.load.image('game-rusalka', RusalkaUrl);
    this.load.image('game-trzmiel', TrzmielUrl);
    this.load.audio(GameSettings.InstructionMessage.Audio.key, GameSettings.InstructionMessage.Audio.src);
    this.gameTimer = new GameTimer(75, 200, 15 * 1000);
    this.scene.add('GameTimer', this.gameTimer, true);
  }

  create() {
    this.initInstruction();

    this.clicked = 0;
    this.clickedSuccess = 4;

    const bgPos = {
      x: 500,
      y: 800
    };

    this.add.image(bgPos.x, bgPos.y, 'game-scene-bg');

    const imageKeys = [
      {
        key: 'game-biedronka',
        x: 390,
        y: 64
      }, {
        key: 'game-biegacz',
        x: 305,
        y: 305
      }, {
        key: 'game-rusalka',
        x: -145,
        y: 90
      }, {
        key: 'game-trzmiel',
        x: 30,
        y: -345
      }
    ];

    imageKeys.forEach(imageKey => {
      let clicked = false;
      const item = this.add.image(bgPos.x + imageKey.x, bgPos.y + imageKey.y, imageKey.key);
      item.setInteractive();
      item.on('pointerdown', () => {
        if (!clicked) {
          clicked = !clicked;
          item.alpha = clicked ? 0.5 : 1;
          this.clicked++;
        }

        if (this.win()) {
          this.gameEnd();
          winScreen(this.gameTimer.getTime(), '1.1.2', 'marcin7111', this);
        }
      });
    });
  }

  update() {
    if (this.gameTimer.isTimeEnded()) {
      this.gameEnd();
      this.scene.start('LoseScene');
    }
  }

  private gameEnd() {
    this.instructionAudio.stop();
    this.scene.remove(this.gameTimer);
  }

  private win() {
    return this.clicked === this.clickedSuccess;
  }

  private initInstruction() {
    const instructionMessage = this.add.text(50, 50, GameSettings.InstructionMessage.Text, {
      fontFamily: 'Arial',
      fontSize: '30px',
      color: '#000',
      wordWrap: {
        width: 800
      }
    });
    instructionMessage.setOrigin(0, 0);

    this.instructionAudio = this.sound.add(GameSettings.InstructionMessage.Audio.key);
    this.instructionAudio.play();
  }
}
