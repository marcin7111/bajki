import Phaser from 'phaser';
import config from './../../shared/config';
import StartScene from './../../shared/scenes/Start';
import WinScene from './../../shared/scenes/Win';
import LoseScene from './../../shared/scenes/Lose';
import GameScene from './Game';

const game = new Phaser.Game(
  config
);

game.scene.add('StartScene', StartScene, true);
game.scene.add('LoseScene', LoseScene);
game.scene.add('WinScene', WinScene);
game.scene.add('GameScene', GameScene);
