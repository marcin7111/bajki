import TimerUrl from '../images/timer.png';

export class GameTimer extends Phaser.Scene {

  private timerEvent: Phaser.Time.TimerEvent;
  private timerText: Phaser.GameObjects.Text;
  private timerProgress: Phaser.GameObjects.Graphics;
  private timerProgressSizes = {
    width: 550,
    height: 60,
  }

  private context: Phaser.Scene;
  private delay: number;
  private x: number;
  private y: number;

  constructor(x: number, y: number, delay: number) {
    super('GameTimer');
    this.x = x;
    this.y = y;
    this.delay = delay;
  }

  preload() {
    this.load.image('timer-image', TimerUrl);
  }

  create() {
    this.timerEvent = this.time.addEvent({
      delay: this.delay
    });

    const timerImage = this.add.image(this.x, this.y, 'timer-image');
    timerImage.setOrigin(0, 0);

    const TimerTextStyle = { fontFamily: 'Arial', fontSize: '123px', color: '#36b8aa', fontStyle: 'italic', align: 'left' };
    this.timerText = this.add.text(this.x + 120, this.y, this.getTime().toFixed(0), TimerTextStyle);
    this.timerText.setOrigin(0, 0);

    const sizes = this.timerProgressSizes;
    const timerProgressBg = this.add.graphics();
    timerProgressBg.lineStyle(10, 0x000000);
    timerProgressBg.strokeRect(this.x + 300, this.y + 33, sizes.width, sizes.height);
    timerProgressBg.fillStyle(0xFFFFFF);
    timerProgressBg.fillRect(this.x + 300, this.y + 33, sizes.width, sizes.height);

    this.timerProgress = this.add.graphics();

    this.updateTime();
  }

  update() {
    this.updateTime();
  }

  updateTime() {
    this.timerText.setText(this.getTime().toFixed(0));
    const sizes = this.timerProgressSizes;
    const progress = 1 - (this.timerEvent.elapsed / this.timerEvent.delay);
    this.timerProgress.clear();
    this.timerProgress.fillStyle(0xdc143c);
    this.timerProgress.fillRect(this.x + 300, this.y + 33, (progress * sizes.width), sizes.height);
  }

  getTime() {
    return Math.ceil((this.timerEvent.delay - this.timerEvent.elapsed) / 1000);
  }

  isTimeEnded() {
    return this.getTime() === 0;
  }
}