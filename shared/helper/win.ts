export function winScreen(score: number, game: string, user: string, context: Phaser.Scene) {
  context.scene.start('WinScene', {
    score,
    game,
    user
  });
}