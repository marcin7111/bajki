export function setText(message: SceneMessage, context: Phaser.Scene) {
  const HeadTextStyle: Phaser.GameObjects.TextStyle = {
    fontFamily: 'Arial',
    fontSize: '40px',
    fontStyle: 'bold',
    color: '#000',
    align: 'center',
    wordWrap: {
      width: 500
    }
  };
  const headText = context.make.text({
    x: 500,
    y: 100,
    text: message.Head,
    origin: { x: 0.5, y: 0 },
    style: HeadTextStyle
  });
  const headTextBounds = headText.getBounds();

  const TextStyle: Phaser.GameObjects.TextStyle = { fontFamily: 'Arial', fontSize: '30px', color: '#000' };
  const TextLines: Phaser.GameObjects.Text[] = [];
  message.TextLines.forEach((textLine, index) => {
    let textY = headTextBounds.y + headTextBounds.height + 20;
    if (TextLines[index - 1]) {
      const bounds = TextLines[index - 1].getBounds();
      textY = bounds.y + bounds.height + 20;
    }
    const text = context.add.text(500, textY, textLine, TextStyle);
    text.setOrigin(0.5, 0);
    TextLines.push(text);
  });
}