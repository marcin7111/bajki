/// <reference path="./../../@types/index.d.ts" />

import Phaser from 'phaser';
import { setText } from '../helper/text';
import StartSceneBgUrl from '../images/start-scene-bg.png';
import ExitButtonUrl from '../images/exit-button.png';
import StartButtonUrl from '../images/start-button.png';
declare const MainSettings: IMainSettings;

export default class StartScene extends Phaser.Scene {
  constructor() {
    super('StartScene');
  }

  preload() {
    this.load.image('start-scene-bg', StartSceneBgUrl);
    this.load.image('exit-button', ExitButtonUrl);
    this.load.image('start-button', StartButtonUrl);
    this.load.audio(MainSettings.StartSceneMessage.Audio.key, MainSettings.StartSceneMessage.Audio.src);
  }

  create() {
    this.add.image(500, 650, 'start-scene-bg');

    const sound = this.sound.add(MainSettings.StartSceneMessage.Audio.key);
    sound.play();

    const exitButton = this.add.image(730, 992, 'exit-button');
    exitButton.setInteractive();
    exitButton.on('pointerdown', () => {
      sound.stop();
      window.location.href = MainSettings.ExitButtonUrl;
    });

    const startButton = this.add.image(730, 1160, 'start-button');
    startButton.setInteractive();
    startButton.on('pointerdown', () => {
      sound.stop();
      this.scene.start('GameScene');
    });


    setText(MainSettings.StartSceneMessage, this);
  }
}
