/// <reference path="./../../@types/index.d.ts" />

import Phaser from 'phaser';
import { setText } from '../helper/text';
import WinSceneBgUrl from '../images/win-scene-bg.png';
import ExitButtonUrl from '../images/exit-button.png';
import StartButtonUrl from '../images/start-button.png';
import QRCode from 'qrcode';
declare const MainSettings: IMainSettings;

export default class WinScene extends Phaser.Scene {
  constructor() {
    super('WinScene');
  }

  score: number;
  gameName: string;
  user: string;

  init(data: any) {
    this.score = data.score;
    this.gameName = data.game;
    this.user = data.user;
  }

  preload() {
    this.load.image('win-scene-bg', WinSceneBgUrl);
    this.load.image('exit-button', ExitButtonUrl);
    this.load.image('start-button', StartButtonUrl);
  }

  create() {
    this.add.image(500, 650, 'win-scene-bg');

    const exitButton = this.add.image(280, 992, 'exit-button');
    exitButton.setInteractive();
    exitButton.on('pointerdown', () => {
      window.location.href = MainSettings.ExitButtonUrl;
    });

    const startButton = this.add.image(280, 1160, 'start-button');
    startButton.setInteractive();
    startButton.on('pointerdown', () => {
      this.scene.start('GameScene');
    });

    setText(MainSettings.WinSceneMessage, this);

    const scoreText = this.add.text(500, 270, this.score.toString(), {
      fontFamily: 'Arial',
      fontSize: '90px',
      fontStyle: 'Bold',
      color: '#000'
    });
    scoreText.setOrigin(0.5, 0);
    
    this.setQrCode(`http://wp.pl?data=${this.getHashedData()}`);
  }

  private getHashedData() {
    return btoa(JSON.stringify({
      user: this.user,
      score: this.score,
      game: this.gameName
    }));
  }

  private setQrCode(url: string) {
    this.textures.remove('qr-code');

    QRCode.toDataURL(url, {
      width: 128,
      height: 128,
      margin: 0
    })
      .then(data => {
        this.textures.once('addtexture', () => {
          this.add.image(500, 450, 'qr-code');
        });
        this.textures.addBase64('qr-code', data);
      });
  }
}
