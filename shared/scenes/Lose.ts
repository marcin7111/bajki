/// <reference path="./../../@types/index.d.ts" />

import Phaser from 'phaser';
import { setText } from '../helper/text';
import LoseSceneBg from '../images/lose-scene-bg.png';
import ExitButtonUrl from '../images/exit-button.png';
import StartButtonUrl from '../images/start-button.png';
declare const MainSettings: IMainSettings;

export default class LoseScene extends Phaser.Scene {
  constructor() {
    super('LoseScene');
  }

  preload() {    
    this.load.image('lose-scene-bg', LoseSceneBg);
    this.load.image('exit-button', ExitButtonUrl);
    this.load.image('start-button', StartButtonUrl);
  }

  create() {
    this.add.image(500, 650, 'lose-scene-bg');

    const exitButton = this.add.image(730, 992, 'exit-button');
    exitButton.setInteractive();
    exitButton.on('pointerdown', () => {
      window.location.href = MainSettings.ExitButtonUrl;
    });

    const startButton = this.add.image(730, 1160, 'start-button');
    startButton.setInteractive();
    startButton.on('pointerdown', () => {
      this.scene.start('GameScene');
    });

    setText(MainSettings.LoseSceneMessage, this);
  }
}
