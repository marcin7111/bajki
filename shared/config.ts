/// <reference path="./../@types/index.d.ts" />

import Phaser from 'phaser';
import './style.css';

export default {
  type: Phaser.AUTO,
  parent: 'game',
  backgroundColor: '#FFF',
  scale: {
    width: 1000,
    height: 1300,
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH
  }
};
